# Build

> docker build -t gitlab-registry.cern.ch/agonzale/cc7-atlassian .

# Push

> docker login gitlab-registry.cern.ch

> docker push gitlab-registry.cern.ch/agonzale/cc7-atlassian

