# Based on the Fedora image created by Matthew Miller.
FROM gitlab-registry.cern.ch/linuxsupport/cc7-base 
# Install nodejs and npm packages.
ENV SDKV=6.2.4
RUN yum update -y
RUN yum install -y wget java-1.8.0-oracle-devel git rpm-build make
RUN wget http://sdkrepo.atlassian.com/rpm-stable/atlassian-plugin-sdk-6.2.4.noarch.rpm
RUN yum localinstall -y atlassian-plugin-sdk-$SDKV.noarch.rpm
# Clean up
RUN yum clean all
